GPD Micro PC
============

Inspired by
-----------

-   <https://github.com/joshskidmore/gpd-micropc-arch-guide>
-   <https://wiki.debian.org/InstallingDebianOn/GPD/Pocket>

Solved
------

-   Usable keyboard on boot
-   Working display
-   Display brightness
-   Rotate screen on boot
-   Rotate screen on login

Still unsolved
--------------

-   Keyboard buttons
    -   keyboard light
-   No audio on headphones jack (jacksense)

Install Debian Buster
---------------------

This was straight forward.

-   Create an USB boot medium with Debian Buster
-   LUKS full encryption
-   Several partitions for /, /home, /var, /tmp
-   Enable non-free sources.
-   xfce desktop
-   ssh-server so we can continue remote.

An external USB Keyboard helps entering the LUKS passphrase as the
internal keyboard is not usable at first ([see later](#keyboard)).

i915 firmware
-------------

    apt install firmware-linux firmware-iwlwifi firmware-realtek

Get additional files which are missing on executing *update-initramfs
-u* from

    cd /lib/firmware/i915/
    wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/i915/bxt_huc_ver01_8_2893.bin
    wget https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/i915/icl_dmc_ver1_07.bin
    cd ~

Display
-------

### Xorg packages,\...

    apt install xrandr arandr xserver-xorg-video-intel

### Linux kernel

To make the display work we need a \>=5.1 kernel:

    apt install build-essential linux-source bc kmod cpio flex cpio libncurses-dev libelf-dev

Get the kernel sources and unpack them:

    apt install xz
    wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.2.6.tar.xz
    xz -d linux-5.2.6.tar.xz

Get the signature and RSA key for verification

    wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.2.6.tar.sign
    gpg --verify linux-5.2.6.tar.sign

Grab the public key (with the RSA key of the last command) from the PGP
keyserver in order to verify the signature and verify again

    gpg --recv-keys 647F28654894E3BD457199BE38DBBDC86092693E
    gpg --verify linux-5.2.6.tar.sign

We should [not]{.underline} get **BAD signature**, so we can safely
unpack the tar archive

    tar -xf linux-5.2.6.tar
    cd linux-5.2.6

Copy the *.config* from the actual kernel

    cp -a /boot/config-$(uname -r) .config

Configure the kernel (just save it)

    make menuconfig

Build the Debian package (-j\`nproc) uses as many threads as we have
cpus)

    make -j`nproc` bindeb-pkg 
      

After a while we get the .deb packages in the parent directory and can
install the new kernel:

    dpkg -i ../linux-image-5.2.6_5.2.6-1_amd64.deb

### Rotate the display 90° for boot

    vim /etc/defaults/grub

```
GRUB_CMDLINE_LINUX_DEFAULT="fbcon=rotate:1"
```

### Rotate the display for login

    vim /etc/lightdm/lightdm.conf

```
[Seat:*]
greeter-setup-script=/etc/lightdm/greeter_setup.sh
```

    vim /etc/lightdm/greeter_setup.sh

```
#!/bin/bash
xrandr -o right
exit 0
```

    chmod +x /etc/lightdm/greeter_setup.sh


### Reduce backlight after boot

    vim /etc/rc.local

```
echo "30" >/sys/class/backlight/intel_backlight/brightness
```

Keyboard
--------

Does not work for entering Luks Password during boot.

To make it work we need to load module *battery* on boot:

    vim /etc/initramfs-tools/modules

```
battery
```

Build initramfs
---------------

    update-initramfs -u

Audio jack - no sound
---------------------

The device at the front-audio-jack is not recognized.

    vim /usr/share/pulseaudio/alsa-mixer/paths/analog-output-headphones.conf

```
[Element Front]
; switch = mute
switch = off
```
thermald
--------

    apt install thermald

Some more links & notes
-----------------------

### Limit battery charge on MicroPC

-   <https://www.reddit.com/r/gpdmicropc/comments/cmrcnt/limit_battery_charge_on_micropc/>

### Audio issues

-   <https://www.reddit.com/r/gpdmicropc/comments/cik47s/disable_audio_power_saving_in_linux_opensuse/>
  